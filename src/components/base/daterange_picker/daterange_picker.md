# Daterange Picker

<!-- STORY -->

## Usage

Daterange picker allows users to choose a date range by manually typing the start/end date into the input fields or by using a calendar-like dropdown.
