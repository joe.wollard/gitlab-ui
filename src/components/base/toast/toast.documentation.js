import description from './toast.md';
import examples from './examples';

export default {
  followsDesignSystem: true,
  description,
  examples,
};
