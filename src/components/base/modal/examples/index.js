import ModalBasicExample from './modal.basic.example.vue';

export default [
  {
    name: 'Basic',
    items: [
      {
        id: 'modal-basic',
        name: 'Basic',
        description: 'Basic Modal',
        component: ModalBasicExample,
      },
    ],
  },
];
